# Overview

Nodejs module - adapted version of [`cOMPLEXtYPE.js` library](https://bitbucket.org/workslon/ontojs/src/8367a54d7dbdc4330a750dfdfbbd9a3e27e17867/src/cOMPLEXtYPE.js) written by Prof. Gerd Wagner, BTU Cottbus

You can find its source code [here](https://bitbucket.org/workslon/complextype/src/4d4419c592f9add67783d06c3b37173923926b3c/index.js)

# Install

## ssh

```
npm install git+ssh://git@bitbucket.org/workslon/complextype.git --save
```

## https

```
npm install https://git@bitbucket.org/workslon/complextype.git --save
```

# Usage

## CommonJS

```javascript
var cOMPLEXtYPE = require('cOMPLEXtYPE');

// ...
```

## ES6 Modules

```javascript
import cOMPLEXtYPE from 'cOMPLEXtYPE';

// ...
```