/**
 * @fileOverview  This file contains the definition of the abstract
 * meta-class cOMPLEXtYPE.
 * @author Gerd Wagner
 * @copyright Copyright 2015 Gerd Wagner, Chair of Internet Technology,
 *   Brandenburg University of Technology, Germany.
 * @license The MIT License (MIT)
 */
/**
 * Abstract meta-class subsuming cOMPLEXdATAtYPE and eNTITYtYPE.
 * cOMPLEXtYPE defines the following type features:
 * name, supertype(s), properties, methods (including the
 * predefined methods toDisplayString, toRecord, set and isInstanceOf)
 * TODO: staticMethods
 *
 * By default, properties are mandatory, if there is no declaration slot
 * "optional:true". The property declaration parameter "minCard" is only
 * meaningful for multi-valued properties, for which "maxCard" has been
 * set to a value greater than 1.
 *
 * On cOMPLEXtYPE creation, the constructor argument "methods" may include
 * a custom "validate" method for validating a complex data value or entity
 * not property-wise, but as a whole, returning an error message in case
 * of an data-value-level constraint violation.
 *
 * @constructor
 * @this {cOMPLEXtYPE}
 * @param  slots  The complex type definition slots.
 */
var util = require('gw-util');
var errorTypes = require('error-types');
var eNUMERATION = require('eNUMERATION');
var cOMPLEXtYPEconstraintViolation = errorTypes.cOMPLEXtYPEconstraintViolation;
var NoConstraintViolation = errorTypes.NoConstraintViolation;
var RangeConstraintViolation = errorTypes.RangeConstraintViolation;
var cOMPLEXdATAtYPEconstraintViolation = errorTypes.cOMPLEXdATAtYPEconstraintViolation;
var IntervalConstraintViolation = errorTypes.IntervalConstraintViolation;
var StringLengthConstraintViolation = errorTypes.StringLengthConstraintViolation;
var CardinalityConstraintViolation = errorTypes.CardinalityConstraintViolation;
var PatternConstraintViolation = errorTypes.PatternConstraintViolation;
var MandatoryValueConstraintViolation = errorTypes.MandatoryValueConstraintViolation;

function cOMPLEXtYPE( slots) {
  var k=0, keys=[], supertype=null,
      featureMaps = {properties:{}, methods:{}, staticMethods:{}};
  this.name = slots.name;
  Object.keys( slots.properties).forEach( function (prop) {
    var propDeclParams = slots.properties[prop],  // the property declaration slots
        minCard = propDeclParams.minCard,
        maxCard = propDeclParams.maxCard;
    // check if cardinality constraints are meaningful
    if (minCard !== undefined) {
      if (!Number.isInteger( minCard) || minCard < 0)
      throw new cOMPLEXtYPEconstraintViolation(
          "minCard value for "+ prop +" ("+ JSON.stringify(maxCard)) +
          ") is invalid (not a non-negative integer)!";
      else if (maxCard === undefined)
        throw new cOMPLEXtYPEconstraintViolation(
            "A minCard value for "+ prop +" is only meaningful when maxCard is defined!");
      else if ((!Number.isInteger( maxCard) || maxCard < 2) && maxCard !== Infinity)
      throw new cOMPLEXtYPEconstraintViolation(
        "Invalid maxCard value for "+ prop +": "+ JSON.stringify(maxCard));
      else if (maxCard < minCard)
        throw new cOMPLEXtYPEconstraintViolation(
            "maxCard value for "+ prop +" is less than mincCard value! ");
    }
    // add default property descriptors
    featureMaps.properties[prop] = util.mergeObjects(
        {writable: true, enumerable: true}, propDeclParams);
  }, this);
  slots.properties = featureMaps.properties;  // completed own properties
  if (slots.supertype || slots.supertypes) {  // non-root class
    if (slots.supertype) {  // only one direct supertype
      this.supertype = slots.supertype;
      Object.keys( featureMaps).forEach( function (fMK) {  // fMK = featureMapKey
        // assign featureMap by merging the supertype's featureMap with own featureMap
        if (slots[fMK] || slots.supertype[fMK]) {
          this[fMK] = util.mergeObjects( slots.supertype[fMK], slots[fMK]);
        }
      }, this);  // passing the context object reference to the forEach function
    } else {  // multiple direct supertypes
      this.supertypes = slots.supertypes;
      // collect features for all featureMaps of all direct supertypes
      for (k=0; k < slots.supertypes.length; k++) {
        supertype = slots.supertypes[k];
        Object.keys( featureMaps).forEach( function (fMK) {
          if (supertype[fMK]) {
            featureMaps[fMK] = util.mergeObjects( featureMaps[fMK], supertype[fMK]);
          }
        });
      }
      Object.keys( featureMaps).forEach( function (fMK) {
        // assign merged featureMaps to object
        if (featureMaps[fMK] || slots[fMK]) {
          this[fMK] = util.mergeObjects( featureMaps[fMK], slots[fMK]);
        }
      }, this);  // passing the context object reference to the forEach function
    }
  } else {  // root class (no inheritance)
    Object.keys( featureMaps).forEach( function (fMK) {
      // assign merged featureMaps to object
      if (slots[fMK]) this[fMK] = slots[fMK];
    }, this);  // passing the context object reference to the forEach function
    if (!this.methods) this.methods = {};
    // add predefined methods such that they will be inherited by all subtypes
    // **************************************************
    this.methods.toString = function () {
    // **************************************************
      var str = this.type.name + "{ ";
      Object.keys( this).forEach( function (key,i) {
        if (key !== "type" && this[key] !== undefined) {
          str += (i>0 ? ", " : "") + key +": "+ this[key];
        }
      }, this);  // pass context object reference
      return str +"}";
    };
    // **************************************************
    this.methods.set = function ( prop, val) {
    // **************************************************
      // this = object
      var constrViol = this.type.check( prop, val);
      if (constrViol instanceof NoConstraintViolation) {
        this[prop] = constrViol.checkedValue;
      } else {
        throw constrViol;
      }
    };
    // **************************************************
    this.methods.isInstanceOf = function (DataType) {
    // **************************************************
      if (!this.type) return false;  // not an object created by a factory
      else return (this.type === DataType) ? true :
                    this.type.isSubTypeOf( DataType);
    };
  }
}
/**
 * Check if this type is a subtype of another type.
 * @method
 * @author Gerd Wagner
 * @param {cOMPLEXtYPE} Class  The class to be tested as superclass.
 * @return {boolean}
 */
cOMPLEXtYPE.prototype.isSubTypeOf = function (DataType) {
  if (!this.supertype && !this.supertypes) return false;
  if (this.supertype) {
    return (this.supertype === DataType) ? true :
                  this.supertype.isSubTypeOf( DataType);
  } else {
    return (this.supertypes.indexOf( DataType) > -1) ? true :
                this.supertypes.some( function (sT) {return sT.isSubTypeOf( DataType);});
  }
};
/**
 * Determine if a property is integer-valued.
 * @method
 * @author Gerd Wagner
 * @param {cOMPLEXdATAtYPE,name|eNUMERATION|cOMPLEXdATAtYPE} T  The type to be checked.
 * @return {boolean}
 */
cOMPLEXtYPE.isIntegerType = function (T) {
  return (typeof(T)==="string" &&
  ["Integer", "NonNegativeInteger","PositiveInteger"].indexOf( T) > -1 ||
  T instanceof eNUMERATION);
};
/**
 * Generic method for checking the integrity constraints defined in property declarations.
 * The values to be checked are first parsed/deserialized if provided as strings.
 *
 * min/max: numeric (or string length) minimum/maximum
 * optional: true if property is single-valued and optional (false by default)
 * range: String|NonEmptyString|Integer|...
 * pattern: a regular expression to be matched
 * minCard/maxCard: minimum/maximum cardinality of a multi-valued property
 *     By default, maxCard is 1, implying that the property is single-valued, in which
 *     case minCard is meaningless/ignored. maxCard may be Infinity.
 *
 * @method
 * @author Gerd Wagner
 * @param {string} prop  The property for which a value is to be checked.
 * @param {string|number|boolean|object} val  The value to be checked.
 * @return {ConstraintViolation}  The constraint violation object.
 */
cOMPLEXtYPE.prototype.check = function (prop, val) {
  var propDeclParams = this.properties[prop],
      constrVio=null, valuesToCheck=[], i=0, keys=[], msg="", label="",
      minCard = 0,  // by default, a multi-valued property is optional
      maxCard = 1,  // by default, a property is single-valued
      card, min=0, max, range, pattern="";
  if (propDeclParams) {
    range = propDeclParams.range;
    min = propDeclParams.min || min;
    max = propDeclParams.max;
    minCard = propDeclParams.minCard || minCard;
    maxCard = propDeclParams.maxCard || maxCard;
    pattern = propDeclParams.pattern;
    msg = propDeclParams.patternMessage;
    label = propDeclParams.label || prop;
  } else {
    return new cOMPLEXtYPEconstraintViolation(
        "Property to be checked ("+ prop +
        ") is not among the type's properties: "+
        Object.keys( this.properties).toString());
  }
  if (val === undefined || val === "") {
    if (propDeclParams.optional) return new NoConstraintViolation();
    else {
      return new MandatoryValueConstraintViolation(
          "A value for "+ prop +" is required!");
    }
  }
  if (maxCard === 1) {  // single-valued property
    valuesToCheck = [val];
  } else {  // multi-valued property
    // can be array-valued or map-valued
    if (Array.isArray( val) ) {
      valuesToCheck = val;
    } else if (typeof( val) === "string") {
      valuesToCheck = val.split(",").map(function (el) {
        return el.trim();
      });
    } else {
      return new RangeConstraintViolation("Values for "+ prop +
      " must be arrays!");
    }
  }
  // convert integer strings to integers
  if (cOMPLEXtYPE.isIntegerType( range)) {
    valuesToCheck.forEach( function (v,i) {
      if (typeof v === 'string' && /^\d+$/.test( v)) {
        valuesToCheck[i] = parseInt( v);
      }
    });
  }
  /*********************************************************************
   ***  Convert value strings to values and check range constraints ****
   ********************************************************************/
  switch (range) {
  case "String":
    valuesToCheck.forEach( function (v) {
      if (typeof(v) !== "string") {
        constrVio = new RangeConstraintViolation("Values for "+ prop +
          " must be strings!");
      }
    });
    break;
  case "NonEmptyString":
    valuesToCheck.forEach( function (v) {
      if (typeof(v) !== "string" || v.trim() === "") {
        constrVio = new RangeConstraintViolation("Values for "+ prop +
            " must be non-empty strings!");
      }
    });
    break;
  case "Integer":
    valuesToCheck.forEach( function (v) {
      if (!Number.isInteger(v)) {
        constrVio = new RangeConstraintViolation("The value of "+ prop +
            " must be an integer!");
      }
    });
    break;
  case "NonNegativeInteger":
    valuesToCheck.forEach( function (v) {
      if (!Number.isInteger(v) || v < 0) {
        constrVio = new RangeConstraintViolation("The value of "+ prop +
            " must be a non-negative integer!");
      }
    });
    break;
  case "PositiveInteger":
    valuesToCheck.forEach( function (v) {
      if (!Number.isInteger(v) || v < 1) {
        constrVio = new RangeConstraintViolation("The value of "+ prop +
            " must be a positive integer (and not "+ v +")!");
      }
    });
    break;
  case "Decimal":
    valuesToCheck.forEach( function (v,i) {
      if (typeof( v) === "string") valuesToCheck[i] = parseFloat( v);
      if (typeof(v) !== "number") {
        constrVio = new RangeConstraintViolation("The value of "+ prop +
            " must be a (decimal) number!");
      }
    });
    break;
  case "Boolean":
    valuesToCheck.forEach( function (v) {
      if (typeof(v) !== "boolean") {
        constrVio = new RangeConstraintViolation("The value of "+ prop +
            " must be either 'true' or 'false'!");
      }
    });
    break;
  case "Date":
    valuesToCheck.forEach( function (v) {
      if (typeof(v) === "string" &&
          (/\d{4}-(0\d|1[0-2])-([0-2]\d|3[0-1])/.test(v) ||
          !isNaN( Date.parse(v)))) {
        valuesToCheck[i] = new Date(v);
      } else if (!(v instanceof Date)) {
            constrVio = new RangeConstraintViolation("The value of "+ prop +
                " must be either a Date value or an ISO date string. "+
                val +" is not admissible!");
        }
    });
    break;
  default:
    if (range instanceof eNUMERATION) {
      valuesToCheck.forEach( function (v) {
        if (!Number.isInteger( v) || v < 1 || v > range.MAX) {
          constrVio = new RangeConstraintViolation("The value "+ v +
              " is not an admissible enumeration integer for "+ prop);
        }
      });
    } else if (range instanceof cOMPLEXdATAtYPE) {
      valuesToCheck.forEach( function (v,i) {
        try {
          valuesToCheck[i] = range.create(v);
        } catch (e) {
          constrVio = new cOMPLEXdATAtYPEconstraintViolation("in "+ range.name +
              ": "+ e.constructor.name +": "+ e.message);
        }
      });
    } else if (Array.isArray( range)) {
      // *** Ad-hoc enumeration ***
      valuesToCheck.forEach( function (v) {
        if (range.indexOf(v) === -1) {
          constrVio = new RangeConstraintViolation("The "+ prop +" value "+ v +
              " is not in value list "+ range.toString());
        }
      });
    } else if (typeof(range) == "function") {
      // constructor-based class
      valuesToCheck.forEach( function (v) {
        if (!(v instanceof range)) {
          constrVio = new RangeConstraintViolation("The value of "+ prop +
              " must be a(n) "+ range.name +"! "+ JSON.stringify(v) +" is not admissible!");
        }
      });
    }
  }
  // return constraint violation found in range switch
  if (constrVio) return constrVio;

  /********************************************************
   ***  Check constraints that apply to several ranges  ***
   ********************************************************/
  if (range === "String" || range === "NonEmptyString") {
    valuesToCheck.forEach( function (v) {
      if (min !== undefined && v.length < min) {
        constrVio = new StringLengthConstraintViolation("The length of "+
            prop + " must not be smaller than "+ min);
      } else if (max !== undefined && v.length > max) {
        constrVio = new StringLengthConstraintViolation("The length of "+
            prop + " must not be greater than "+ max);
      } else if (pattern !== undefined && !pattern.test( v)) {
          constrVio = new PatternConstraintViolation( msg || v +
              "does not comply with the pattern defined for "+ prop);
      }
    });
  }
  if (range === "Integer" || range === "NonNegativeInteger" ||
      range === "PositiveInteger") {
    valuesToCheck.forEach( function (v) {
      if (min !== undefined && v < min) {
        constrVio = new IntervalConstraintViolation( prop +
            " must be greater than "+ min);
      } else if (max !== undefined && v > max) {
        constrVio = new IntervalConstraintViolation( prop +
            " must be smaller than "+ max);
      }
    });
  }
  if (constrVio) return constrVio;

  /********************************************************
   ***  Check cardinality constraints  *********************
   ********************************************************/
  if (maxCard > 1) { // (a multi-valued property can be array-valued or map-valued)
    // check minimum cardinality constraint
    if (minCard > 0 && valuesToCheck.length < minCard) {
      return new CardinalityConstraintViolation("A collection of at least "+
          minCard +" values is required for "+ prop);
    }
    // check maximum cardinality constraint
    if (valuesToCheck.length > maxCard) {
      return new CardinalityConstraintViolation("A collection value for "+
          prop +" must not have more than "+ maxCard +" members!");
    }
  }
  val = maxCard === 1 ? valuesToCheck[0] : valuesToCheck;
  return new NoConstraintViolation( val);
};

module.exports = cOMPLEXtYPE;